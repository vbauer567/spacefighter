
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	//Checks if the Enemy ship has a spawn delay and if so it will delay it or spawn it on begining
	if (m_delaySeconds > 0)
	{	
		m_delaySeconds -= pGameTime->GetTimeElapsed();

		if (m_delaySeconds <= 0)
		{
			GameObject::Activate(); // activates the enemy game object
		}
	}

	if (IsActive())
	{
		m_activationSeconds += pGameTime->GetTimeElapsed();
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate(); // if the time activated is greater than 2 and the object is not on screen it will deactivate the chosen object
	}

	Ship::Update(pGameTime); //updates anything that has occured, collisions, fires, and what not
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position); //Gets the position of the enemy ship from the math vector2
	m_delaySeconds = delaySeconds; //Sets the delay time from the preset  delay time

	Ship::Initialize(); //initializes the enemy ship
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage); //checks if ship is damageable and if so will check the health points to determin whether to deactivate upon taking damage
}